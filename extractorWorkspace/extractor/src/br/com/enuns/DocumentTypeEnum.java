package br.com.enuns;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author bruno.dourado
 *
 */
public enum DocumentTypeEnum {
	
	DPR  			  ("11100", "DPR"      ), 
	ASDPR			  ("11110", "ASDPR"    ), 
	ASCOM			  ("11120", "ASCOM"    ), 
	GEJUR			  ("11130", "GEJUR"    ),
	GPLAN			  ("11140", "GPLAN"    ),
	GQSMS			  ("11150", "GQSMS"    ),
	GEGRC			  ("11160", "GEGRC"    ),
	DAF  			  ("11300", "DAF"  	   ),
	GERAD			  ("11310", "GERAD"    ),
	GECBS			  ("11320", "GECBS"    ),
	GETIN			  ("11330", "GETIN"    ),
	GEREH			  ("11340", "GEREH"    ),
	GECOT			  ("11350", "GECOT"    ),
	GEFIN			  ("11360", "GEFIN"    ),
	ASDAF			  ("11370", "ASDAF"    ),
	DTC  			  ("11400", "DTC"      ),
	GEPES			  ("11410", "GEPES"    ),
	GECMO			  ("11420", "GECMO"    ),
	GEPRO			  ("11430", "GEPRO"    ),
	GEOPE			  ("11440", "GEOPE"    ),
	GEMAC			  ("11450", "GEMAC"    ),
	GEIND			  ("11460", "GEIND"    ),
	GEMUV			  ("11470", "GEMUV"    ),
	ASDTC			  ("11480", "ASDTC"	   ),
	ASREG			  ("11510", "ASREG"	   ),
	AUDITORIA_INTERNA ("12102", "AUDITORIA");
    
    private final String code;
    private final String description;
    
    DocumentTypeEnum(String code, String description) {
    	this.code = code;
    	this.description = description;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getDescription() {
        return description;
    }
    
    public static DocumentTypeEnum getByCodeDescription(String code, String description) {
    	for (DocumentTypeEnum typeDocumentEnum : DocumentTypeEnum.getList()) {
			if (typeDocumentEnum.getCode().compareTo(code) == 0
			 && typeDocumentEnum.getCode().compareTo(description) == 0) {
				return typeDocumentEnum;
			}
		}
    	return null;
    }
    
    public static List<DocumentTypeEnum> getList() {
    	return Arrays.asList(DocumentTypeEnum.values());
    }
    
}
