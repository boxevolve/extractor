package br.com.manager;

import java.awt.Component;
import java.io.File;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import br.com.enuns.ExtractType;
import br.com.model.DocumentPage;
import br.com.utils.ConstantsUtils;

/**
 * 
 * @author bruno.dourado
 *
 */
public class InterfaceManager {
	
	private String defaultPath;
	private String directoryPath;
	private String extractPath;
	private String manualExtractPath;
	private String copyPath;
	private String newFilePath;
	private ExtractType extractType;

	public InterfaceManager() {
		super();
	}
	
	/**
	 * Responsavel por importar um arquivo do tipo PDF.
	 * 
	 * @param parent
	 * @return
	 */
	public File fileChooserPdf(Component parent) {
		JFileChooser fileChooser = new JFileChooser(defaultPath);
		fileChooser.setFileFilter(new FileNameExtensionFilter(ConstantsUtils.DESCRIPTION_PDF, ConstantsUtils.PDF));
		return fileChooser(fileChooser, parent);
	}
	
	/**
	 * Responsavel por importar o arquivo.
	 * 
	 * @param fileChooser
	 * @param parent
	 * @return
	 */
	private File fileChooser(JFileChooser fileChooser, Component parent) {
		File file = null;
		if (fileChooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
		  file = fileChooser.getSelectedFile();
		} 
		return file;
	}
	
	/**
	 * Responsavel por selecionar um diretorio.
	 * 
	 * @param parent
	 * @return
	 */
	public String directoryChooser(Component parent) {
		JFileChooser directoryChooser = new JFileChooser();
		directoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		String directoryChooserPath = ConstantsUtils.EMPTY;
		if (directoryChooser.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
			directoryChooserPath = directoryChooser.getSelectedFile().getAbsolutePath();
		} 
		return directoryChooserPath;
	}
	
	/**
	 * Responsavel por criar uma tabela de extra��o automatica.
	 * 
	 * @return
	 */
	public static DefaultTableModel createExtractTable() {
		return new DefaultTableModel(
			new Object[][] {},
			new String[] {
				ConstantsUtils.COST_CENTER, ConstantsUtils.DOCUMENT_NAME, ConstantsUtils.ACTION
		}) {
			private static final long serialVersionUID = 8538401296852423936L;
				
			boolean[] canEdit = new boolean []{ false, false, false };    
	        @Override    
	        public boolean isCellEditable(int rowIndex, int columnIndex) {    
	            return canEdit [columnIndex];    
	        }  
		};
	}
	
	/**
	 * Responsavel por popular a tabela de extra��o automatica.
	 * 
	 * @param table
	 * @param listDocumentPage
	 */
	public static void createExtractRows(JTable table, List<DocumentPage> listDocumentPage) {
		table.setModel(InterfaceManager.createExtractTable());
		for (DocumentPage documentPage : listDocumentPage) {
			InterfaceManager.createExtractRow(table, documentPage.getTypeDocument().getDescription(), 
					documentPage.getFileName());
		}
	}
	
	/**
	 * Responsavel por inserir uma linha na tabela extra��o automatica.
	 * 
	 * @param table
	 * @param costCenter
	 * @param documentName
	 */
	public static void createExtractRow(JTable table, String costCenter, String documentName) {
		Object[] newRow = new Object[]{costCenter, documentName, ConstantsUtils.OPEN_DOCUMENT};
		InterfaceManager.addTableRow(table, newRow);
	}
	
	/**
	 * Responsavel por criar uma tabela de extra��o manual.
	 * 
	 * @return
	 */
	public static DefaultTableModel createManualExtractTable() {
		return new DefaultTableModel(
			new Object[][] {},
			new String[] {
				ConstantsUtils.ADDRESS, ConstantsUtils.NAME, ConstantsUtils.ACTION
		}) {
			private static final long serialVersionUID = -7864152547113326203L;
			
			boolean[] canEdit = new boolean []{ false, false, false };    
	        @Override    
	        public boolean isCellEditable(int rowIndex, int columnIndex) {    
	            return canEdit [columnIndex];    
	        }  
		};
	}
	
	/**
	 * Responsavel por popular a tabela de extra��o manual.
	 * 
	 * @param table
	 * @param listDocumentPage
	 */
	public static void createManualExtractRows(JTable table, List<DocumentPage> listDocumentPage) {
		table.setModel(InterfaceManager.createManualExtractTable());
		for (DocumentPage documentPage : listDocumentPage) {
			InterfaceManager.createManualExtractRow(table, documentPage.getDirectory(), documentPage.getFileName());
		}
	}
	
	/**
	 * Responsavel por inserir uma linha na tabela extra��o manual.
	 * 
	 * @param table
	 * @param directory
	 * @param documentName
	 */
	public static void createManualExtractRow(JTable table, String directory, String documentName) {
		Object[] newRow = new Object[]{directory, documentName, ConstantsUtils.OPEN_FILE};
		InterfaceManager.addTableRow(table, newRow);
	}
	
	/**
	 * Responsavel por inserir uma linha na tabela.
	 * 
	 * @param table
	 * @param objects
	 */
	public static void addTableRow(JTable table, Object ...objects) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.addRow(objects);
	}
	
	public String getDefaultPath() {
		return null == defaultPath ? ConstantsUtils.EMPTY : defaultPath;
	}

	public void setDefaultPath(String defaultPath) {
		this.defaultPath = defaultPath;
	}

	public String getDirectoryPath() {
		return directoryPath;
	}

	public void setDirectoryPath(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	public String getExtractPath() {
		return extractPath;
	}

	public void setExtractPath(String extractPath) {
		this.extractPath = extractPath;
	}

	public String getManualExtractPath() {
		return manualExtractPath;
	}

	public void setManualExtractPath(String manualExtractPath) {
		this.manualExtractPath = manualExtractPath;
	}
	
	public String getCopyPath() {
		return copyPath;
	}

	public void setCopyPath(String copyPath) {
		this.copyPath = copyPath;
	}
	
	public String getNewFilePath() {
		return newFilePath;
	}

	public void setNewFilePath(String newFilePath) {
		this.newFilePath = newFilePath;
	}
	
	public ExtractType getExtractType() {
		return extractType;
	}

	public void setExtractType(ExtractType extractType) {
		this.extractType = extractType;
	}
	
}
