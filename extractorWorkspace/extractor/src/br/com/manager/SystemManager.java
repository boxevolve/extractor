package br.com.manager;

import java.awt.Component;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import br.com.utils.ConstantsUtils;

/**
 * 
 * @author bruno.dourado
 *
 */
public class SystemManager {
	
	private SystemManager() {
		super();
	}
	
	/**
	 * Devido � altera��o do m�dulo de gerenciamento de cores do Java em dire��o 
	 * a �LittleCMS�, os usu�rios podem experimentar um desempenho lento nas 
	 * opera��es em cores. Uma solu��o � desativar o LittleCMS em favor 
	 * do antigo KCMS (Kodak Color Management System)
	 */
	public static void disableLittleCMS() {
		System.setProperty(ConstantsUtils.SUN_JAVA2D_CMM, ConstantsUtils.KCMS_SERVICE_PROVIDER);
	}
	
	/**
	 * Responsavel por abrir o arquivo pelo caminho completo.
	 * 
	 * @param absolutePath
	 * @throws IOException
	 */
	public static void openFile(String absolutePath, Component parent) throws IOException {
		SystemManager.openFile(new File(absolutePath), parent);
	}
	
	/**
	 * Responsavel por abrir o arquivo.
	 * 
	 * @param file
	 * @throws IOException
	 */
	public static void openFile(File file, Component parent) throws IOException {
        if (file.exists()) {
            Desktop.getDesktop().open(file);             
        } else {
        	MessageManager.showMessageDialogError(parent, ConstantsUtils.FILE_NOT_FOUND_MSG, 
        			ConstantsUtils.FILE_NOT_FOUND);
        }
	}
	
	/**
	 * Responsavel por abrir a pasta com o caminho informado.
	 * 
	 * @param path
	 * @throws IOException
	 */
	public static void openFolder(String path) throws IOException {
		StringBuilder command = new StringBuilder();
		command.append(ConstantsUtils.EXPLORER);
		command.append(ConstantsUtils.SPACE);
		command.append(path);
	    Runtime.getRuntime().exec(command.toString());
	}
	
}
