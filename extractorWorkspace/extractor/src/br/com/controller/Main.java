package br.com.controller;

import br.com.dto.MainDTO;
import br.com.manager.InterfaceManager;
import br.com.manager.MessageManager;
import br.com.manager.PDFManager;
import br.com.manager.SystemManager;
import br.com.ui.ViewUI;

/**
 * 
 * @author bruno.dourado
 *
 */
public class Main {

	public static void main(String[] args) {
		
		SystemManager.disableLittleCMS();
		MainDTO mainDTO = new MainDTO(new InterfaceManager(), new PDFManager());
		
		try {
			ViewUI window = new ViewUI(mainDTO);
			window.getFrame().setVisible(true);
		} catch (Exception e) {
			MessageManager.showExceptionDialogError(null);
		}
		
	}

}
