package br.com.enuns;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author bruno.dourado
 *
 */
public enum ExtractType {
	
	ONLY     ("0", "�nico"    ), 
	INTERVAL ("1", "Intervalo"), 
	MULTIPLE ("2", "Multiplo" );
    
    private final String code;
    private final String description;
    
    ExtractType(String code, String description) {
    	this.code = code;
    	this.description = description;
    }
    
    public String getCode() {
        return code;
    }
    
    public String getDescription() {
        return description;
    }
    
    public static ExtractType getByCodeDescription(String code, String description) {
    	for (ExtractType typeDocumentEnum : ExtractType.getList()) {
			if (typeDocumentEnum.getCode().compareTo(code) == 0
			 && typeDocumentEnum.getCode().compareTo(description) == 0) {
				return typeDocumentEnum;
			}
		}
    	return null;
    }
    
    public static List<ExtractType> getList() {
    	return Arrays.asList(ExtractType.values());
    }

}
