package br.com.manager;

import java.awt.Component;

import javax.swing.JOptionPane;

import br.com.dto.MainDTO;
import br.com.utils.ConstantsUtils;

/**
 * 
 * @author bruno.dourado
 *
 */
public class MessageManager {

	private MessageManager() {
		super();
	}
	
	/**
	 * Apresenta mensagem de sucesso ao usuario com t�tulo padr�o.
	 * 
	 * @param parent
	 * @param message
	 */
	public static void showMessageDialogSuccess(Component parent, String message) {
		MessageManager.showMessageDialogSuccess(parent, message, null);
	}
	
	/**
	 * Apresenta mensagem de sucesso ao usuario.
	 * 
	 * @param parent
	 * @param message
	 * @param title
	 */
	public static void showMessageDialogSuccess(Component parent, String message, String title) {
		JOptionPane.showMessageDialog(parent, message, null == title ? ConstantsUtils.SUCCESS : title, 
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * Apresenta mensagem de aten��o ao usuario com t�tulo padr�o.
	 * 
	 * @param parent
	 * @param message
	 */
	public static void showMessageDialogWarning(Component parent, String message) {
		MessageManager.showMessageDialogWarning(parent, message, null);
	}
	
	/**
	 * Apresenta mensagem de aten��o ao usuario.
	 * 
	 * @param parent
	 * @param message
	 * @param title
	 */
	public static void showMessageDialogWarning(Component parent, String message, 
			String title) {
		JOptionPane.showMessageDialog(parent, message, null == title ? ConstantsUtils.ATTENTION : title, 
				JOptionPane.WARNING_MESSAGE);
	}
	
	/**
	 * Apresenta mensagem de erro ao usuario com t�tulo padr�o.
	 * 
	 * @param parent
	 * @param message
	 */
	public static void showMessageDialogError(Component parent, String message) {
		MessageManager.showMessageDialogError(parent, message, null);
	}
	
	/**
	 * Apresenta mensagem de erro ao usuario.
	 * 
	 * @param parent
	 * @param message
	 * @param title
	 */
	public static void showMessageDialogError(Component parent, String message, String title) {
		JOptionPane.showMessageDialog(parent, message, null == title ? ConstantsUtils.ERROR : title, 
				JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Apresenta mensagem padr�o de erro quando ocorre alguma exception no sistema.
	 * 
	 * @param parent
	 */
	public static void showExceptionDialogError(Component parent) {
		MessageManager.showMessageDialogError(parent, ConstantsUtils.UNEXPECTED_ERROR, null);
	}
	
	/**
	 * Responsavel por verificar se existe arquivo importado e notificar ao usuario caso n�o tenha.
	 *  
	 * @param mainDTO
	 * @param parent
	 * @return
	 */
	public static boolean showMessageDialogImport(MainDTO mainDTO, Component parent) {
		if(null == mainDTO.getImportFile()) {
			MessageManager.showMessageDialogImport(parent);
			return false;
		}
		return true;
	}
	
	/**
	 * Apresenta mensagem padr�o de aviso que n�o foi importado arquivo.
	 * 
	 * @param parent
	 */
	public static void showMessageDialogImport(Component parent) {
		JOptionPane.showMessageDialog(parent, ConstantsUtils.NOT_IMPORTED, ConstantsUtils.ATTENTION, 
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void showMessageDialogLoading(Component parent) {
		parent.setEnabled(false);
		JOptionPane.showMessageDialog(parent, ConstantsUtils.WAIT_TO_PROCESS, ConstantsUtils.LOADING, 
				JOptionPane.INFORMATION_MESSAGE);
	}
	
}
