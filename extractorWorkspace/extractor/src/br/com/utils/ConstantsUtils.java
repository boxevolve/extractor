package br.com.utils;

/**
 * 
 * @author bruno.dourado
 *
 */
public class ConstantsUtils {
	
	private ConstantsUtils() {
		super();
	}
	
	/**
	 * Constantes de sistema.
	 */
	public static final String APPLICATION_NAME = "Extractor";
	public static final String EMPTY = "";
	public static final String SEPARATOR = "\\";
	public static final String SUN_JAVA2D_CMM = "sun.java2d.cmm";
	public static final String KCMS_SERVICE_PROVIDER = "sun.java2d.cmm.kcms.KcmsServiceProvider";
	public static final String USER_DIR = "user.dir";
	public static final String EXIT = "Sair";
	public static final String LOOK_AND_FELL = "Nimbus";
	public static final String EXPLORER = "explorer";
	public static final String FLAG_LINE_RESUMIDO = "*";
	public static final String FLAG_SPLIT_MULTIPLE = ",";
	public static final String NEXT_LINE = "\\n";
	public static final String SPACE = " ";
	public static final String MODE_RANDOM_ACCESS_FILE = "r";
	
	/**
	 * Constantes de arquivo.
	 */
	public static final String PDF_EXTENSION = ".pdf";
	public static final String DESCRIPTION_PDF = "PDF (*.pdf)";
	public static final String PDF = "pdf";
	
	/**
	 * Constantes de nomenclatura.
	 */
	public static final String DEFAULT_COPY = "_copia";
	public static final String DEFAULT_NEW_FILE = "novo_arquivo";
	public static final String TOTAL_COST_CENTER = "_total_centro_de_custo";
	public static final String RAMAL = "_ramal";
	public static final String EXTRACT_ONLY = "_�nica";
	public static final String EXTRACT_INTERVAL = "_intervalo";
	public static final String EXTRACT_MULTIPLE = "_m�ltiplo";
	
	/**
	 * Constantes de tabela 
	 */
	public static final String ACTION = "A��o";
	public static final String ADDRESS = "Endere�o";
	public static final String NAME = "Nome";
	public static final String COST_CENTER = "Centro de custo";
	public static final String DOCUMENT_NAME = "Nome do documento";
	public static final String OPEN_DOCUMENT = "Abrir Documento";
	public static final String OPEN_FILE = "Abrir Arquivo";
	
	/**
	 * Constantes de t�tulos.
	 */
	public static final String ATTENTION = "Aten��o!";
	public static final String ERROR = "Erro!";
	public static final String SUCCESS = "Sucesso!";
	public static final String LOADING = "Carregando!";
	public static final String FILE_NOT_FOUND = "Arquivo n�o encontrado!";
	
	/**
	 * Constantes de mensagens.
	 */
	public static final String UNEXPECTED_ERROR = "Ocorreu um erro inesperado!";
	public static final String PAGE_NOT_FOUND = "P�gina n�o encontrada!";
	public static final String INTERVAL_ERROR = "A p�gina final n�o pode ser menor que a inicial!";
	public static final String INVALID_FORMAT = "Formato inv�lido!";
	public static final String NOT_IMPORTED = "Nenhum documento foi importado...";
	public static final String WAIT_TO_PROCESS = "Aguarde enquanto esta sendo processado...";
	public static final String FILE_NOT_FOUND_MSG = "Arquivo movido ou removido do sistema!";
	public static final String CREATED_NEW_FILE = "Novo arquivo criado com sucesso!";
	
}
