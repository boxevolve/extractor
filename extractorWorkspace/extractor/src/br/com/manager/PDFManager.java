package br.com.manager;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.text.PDFTextStripper;

import br.com.pdfbox.PdfBoxManager;
import br.com.utils.ConstantsUtils;

/**
 * 
 * @author bruno.dourado
 *
 */
public class PDFManager {
	
	private PDFParser parser;
	private PDFTextStripper pdfStripper;
	private PDDocument pdDoc;
	private COSDocument cosDoc;
	
	private String text;
	private String filePath;
	private File file;

	public PDFManager() {
		super();
	}
	
	/**
	 * Responsavel por chamar o metodo que ira criar um arquivo PDF com o nome 
	 * informado e utilizando o documento do proprio objeto.
	 * 
	 * @param fileName
	 * @throws IOException 
	 */
	public void createFile(String fileName) throws IOException {
		createFile(ConstantsUtils.EMPTY, fileName, getPdDoc(), Collections.emptyList());
	}
	
	/**
	 * Responsavel por chamar o metodo que ira criar um arquivo PDF com o nome 
	 * e o documento informado.
	 * 
	 * @param fileName
	 * @throws IOException 
	 */
	public void createFile(String fileName, PDDocument document) throws IOException {
		createFile(ConstantsUtils.EMPTY, fileName, document, Collections.emptyList());
	}
	
	/**
	 * Responsavel por chamar o metodo que ira criar um arquivo PDF com o nome 
	 * e o caminho informado.
	 * 
	 * @param fileName
	 * @throws IOException 
	 */
	public void createFile(String fileName, String path) throws IOException {
		createFile(path, fileName, getPdDoc(), Collections.emptyList());
	}
	
	/**
	 * Responsavel por criar um arquivo PDF com o nome, caminho e documento informado.
	 * 
	 * @param path
	 * @param fileName
	 * @param document
	 * @throws IOException 
	 */
	public void createFile(String path, String fileName, PDDocument document, List<PDPage> pages) throws IOException {
		if (null != pages) {
			for (PDPage pdPage : pages) {
				document.addPage(pdPage);
			}
		}
		document.save(PDFManager.getAbsolutePath(path, fileName));
		document.close();
	}
	
	/**
	 * Respons�vel por chamar o metodo que ira converter o PDF em texto sem limite de paginas.
	 * 
	 * @return
	 * @throws IOException
	 */
	public String toText(Component parent) throws IOException {
		return convertToText(null, null, parent);
	}
	
	/**
	 * Respons�vel por chamar o metodo que ira converter o PDF em texto passando 
	 * um limite de paginas de inicio e fim.
	 * 
	 * @param startPage
	 * @param endPage
	 * @return
	 * @throws IOException
	 */
	public String toText(Integer startPage, Integer endPage, Component parent) throws IOException {
		return convertToText(startPage, endPage, parent);
	}

	/**
	 * Respons�vel por converter o PDF em texto, os parametros "startPage" e "endPage" 
	 * servem para ele converter um limite de paginas.
	 * 
	 * @param startPage
	 * @param endPage
	 * @return
	 * @throws IOException
	 */
	private String convertToText(Integer startPage, Integer endPage, Component parent) throws IOException {
		importFile(startPage, endPage, parent);
		setText(getPdfStripper().getText(getPdDoc()));
		return getText();
	}
	
	/**
	 * Responsavel por importar o arquivo PDF completo.
	 * 
	 * @throws IOException
	 */
	public void importFile(Component parent) {
		importFile(null, null, parent);
	}
	
	/**
	 * Responsavel por importar o arquivo PDF por limite de paginas.
	 * 
	 * @param startPage
	 * @param endPage
	 * @throws IOException
	 */
	public void importFile(Integer startPage, Integer endPage, Component parent) {
		if (null != this.getPdDoc()) {
			try {
				this.getPdDoc().close();
			} catch (Exception e) {
				MessageManager.showExceptionDialogError(parent);
			}
		}
		this.setPdfStripper(null);
		this.setPdDoc(null);
		this.setCosDoc(null);
		
		try {
			setFile(new File(getFilePath()));
			setParser(new PDFParser(new RandomAccessFile(getFile(), ConstantsUtils.MODE_RANDOM_ACCESS_FILE)));
	
			getParser().parse();
			setCosDoc(getParser().getDocument());
			setPdfStripper(new PDFTextStripper());
			setPdDoc(new PDDocument(getCosDoc()));
			getPdDoc().getNumberOfPages();
			
			getPdfStripper().setStartPage(null == startPage ? 1 : startPage);
			getPdfStripper().setEndPage(null == endPage ? getPdDoc().getNumberOfPages() : endPage);
		} catch(Exception e) {
			MessageManager.showExceptionDialogError(parent);
		}
	}

	/**
	 * Responsavel por carregar o arquivo.
	 * 
	 * @param file
	 * @param parent
	 * @throws IOException
	 */
	public void loadDocument(File file, Component parent) throws IOException {
		try {
			if (null != getPdDoc()) {
				getPdDoc().close();
			}
			setPdDoc(PDDocument.load( file, file.getName() ));
		} catch (Exception e) {
			MessageManager.showExceptionDialogError(parent);
		}
	}
	
	/**
	 * Responsavel por criar um novo arquivo com as informa��es que o usuario digitar.
	 * 
	 * @param path
	 * @param fileName
	 * @param parent
	 * @param textContent
	 * @return
	 */
	public static String createNewFile(String path, String fileName, Component parent, String textContent) {
		PDPage page = new PDPage();
		String absolutePath = PDFManager.getAbsolutePath(path, PDFManager.getNameNewFile(fileName));
		try (
		    PDDocument document = new PDDocument();
		) {
			document.addPage(page);
			addContent(parent, page, document, textContent); 
			document.save(absolutePath);
		} catch (Exception e) {
			MessageManager.showExceptionDialogError(parent);
		}
		MessageManager.showMessageDialogSuccess(parent, ConstantsUtils.CREATED_NEW_FILE);
		return absolutePath;
	}

	/**
	 * Adiciona conte�do ao arquivo.
	 * 
	 * @param parent
	 * @param page
	 * @param document
	 * @param textContent
	 */
	private static void addContent(Component parent, PDPage page, PDDocument document, String textContent) {
		try ( 
			PDPageContentStream contentStream = new PDPageContentStream(document, page);
		) {
			PDFont font = PDType1Font.COURIER_BOLD;
			
			contentStream.beginText();
			contentStream.setFont(font, 30);
			contentStream.newLineAtOffset(50, 700);
			contentStream.showText(textContent);
			contentStream.endText();
		} catch (IOException e) {
			MessageManager.showExceptionDialogError(parent);
		}
	}
	
	/**
	 * Teste para criar tabela.
	 * 
	 * @throws IOException
	 */
	public static void createTable() throws IOException {
		try (PDDocument doc = new PDDocument()) {
		    PDPage page = new PDPage();
		    doc.addPage(page);

		    try (PDPageContentStream contentStream = new PDPageContentStream(doc, page)) {
		        String[][] content = {{"a", "b", "1", "R", "R", "R", "R", "R", "R"},
		            {"c", "d", "2", "R", "R", "R", "R", "R", "R"},
		            {"e", "f", "3", "R", "R", "R", "R", "R", "R"},
		            {"g", "h", "4", "R", "R", "R", "R", "R", "R"},
		            {"i", "j", "5", "R", "R", "R", "R", "R", "R"}};
		       PdfBoxManager.drawTable(page, contentStream, 700.0f, 100.0f, content);
		    }
		    doc.save("test.pdf");
		}
	}
	
	/**
	 * Responsavel por gerar um nome padr�o para copia.
	 * 
	 * @param newName
	 * @param originalName
	 * @return
	 */
	public static String getCopyNameDocument(String newName, String originalName) {
		originalName = PDFManager.removeExtension(originalName);
		return null != newName && ConstantsUtils.EMPTY.compareTo(newName) != 0 ? newName 
				: originalName + ConstantsUtils.DEFAULT_COPY;
	}
	
	/**
	 * Responsavel por gerar um nome padr�o para o novo arquivo.
	 * 
	 * @param newName
	 * @param originalName
	 * @return
	 */
	public static String getNameNewFile(String newName) {
		return null != newName && ConstantsUtils.EMPTY.compareTo(newName) != 0 ? newName 
				: ConstantsUtils.DEFAULT_NEW_FILE;
	}
	
	/**
	 * Responsavel por remover a nomenclatura da extens�o do nome do arquivo, por exemplo:
	 * "arquivo.pdf" para "arquivo".
	 * 
	 * @param fileName
	 * @return
	 */
	public static String removeExtension(String fileName) {
		return fileName.replace(ConstantsUtils.PDF_EXTENSION, ConstantsUtils.EMPTY);
	}
	
	/**
	 * Retorna o caminho padr�o caso tenha passado um caminho nulo.
	 * 
	 * @param path
	 * @return
	 */
	public static String getPath(String path) {
		return null == path ? System.getProperty(ConstantsUtils.USER_DIR) : path;
	}
	
	/**
	 * Retorna o caminho completo utilizando o endere�o e o nome do arquivo.
	 * 
	 * @param path
	 * @param fileName
	 * @return
	 */
	public static String getAbsolutePath(String path, String fileName) {
		StringBuilder absolutePath = new StringBuilder();
		absolutePath.append(PDFManager.getPath(path));
		absolutePath.append(ConstantsUtils.SEPARATOR);
		absolutePath.append(fileName);
		absolutePath.append(ConstantsUtils.PDF_EXTENSION);
		return absolutePath.toString();
	}
	
	public PDFParser getParser() {
		return parser;
	}

	public void setParser(PDFParser parser) {
		this.parser = parser;
	}

	public PDFTextStripper getPdfStripper() {
		return pdfStripper;
	}

	public void setPdfStripper(PDFTextStripper pdfStripper) {
		this.pdfStripper = pdfStripper;
	}

	public PDDocument getPdDoc() {
		return pdDoc;
	}

	public void setPdDoc(PDDocument pdDoc) {
		this.pdDoc = pdDoc;
	}

	public COSDocument getCosDoc() {
		return cosDoc;
	}

	public void setCosDoc(COSDocument cosDoc) {
		this.cosDoc = cosDoc;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
}
