package br.com.pdfbox;

import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class PdfBoxManager {
	
	private PdfBoxManager() {
		super();
	}
	
	public static void drawTable(PDPage page, PDPageContentStream contentStream,
	    float y, float margin, String[][] content) throws IOException {
	    final int rows = content.length;
	    final int cols = content[0].length;
	    final float rowHeight = 20.0f;
	    final float tableWidth = page.getMediaBox().getWidth() - 2.0f * margin;
	    final float tableHeight = rowHeight * (float) rows;
	    final float colWidth = tableWidth / (float) cols;

	    //draw the rows
	    float nexty = y ;
	    for (int i = 0; i <= rows; i++) {
	        contentStream.moveTo(margin, nexty);
	        contentStream.lineTo(margin + tableWidth, nexty);
	        contentStream.stroke();
	        nexty-= rowHeight;
	    }

	    //draw the columns
	    float nextx = margin;
	    for (int i = 0; i <= cols; i++) {
	        contentStream.moveTo(nextx, y);
	        contentStream.lineTo(nextx, y - tableHeight);
	        contentStream.stroke();
	        nextx += colWidth;
	    }

	    //now add the text
	    contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12.0f);

	    final float cellMargin = 5.0f;
	    float textx = margin + cellMargin;
	    float texty = y - 15.0f;
	    for (final String[] aContent : content) {
	        for (String text : aContent) {
	            contentStream.beginText();
	            contentStream.newLineAtOffset(textx, texty);
	            contentStream.showText(text);
	            contentStream.endText();
	            textx += colWidth;
	        }
	        texty -= rowHeight;
	        textx = margin + cellMargin;
	    }
	}
	
}
