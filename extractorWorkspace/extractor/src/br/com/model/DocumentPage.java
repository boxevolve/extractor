package br.com.model;

import java.util.List;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import br.com.enuns.DocumentTypeEnum;

/**
 * 
 * @author bruno.dourado
 *
 */
public class DocumentPage implements Comparable<DocumentPage> {
	
	private String fileName;
	private String directory;
	private DocumentTypeEnum typeDocument;
	private List<Integer> listPages;
	
	public DocumentPage() {
		super();
	}
	
	public DocumentPage(DocumentTypeEnum typeDocument) {
		this.typeDocument = typeDocument;
	}
	
	public DocumentPage(DocumentTypeEnum typeDocument, List<Integer> listPages) {
		this.typeDocument = typeDocument;
		this.listPages = listPages;
	}
	
	public DocumentPage(String fileName, String directory, List<Integer> listPages) {
		this.fileName = fileName;
		this.directory = directory;
		this.listPages = listPages;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public DocumentTypeEnum getTypeDocument() {
		return typeDocument;
	}

	public void setTypeDocument(DocumentTypeEnum typeDocument) {
		this.typeDocument = typeDocument;
	}

	public List<Integer> getListPages() {
		return listPages;
	}

	public void setListPages(List<Integer> listPages) {
		this.listPages = listPages;
	}

	@Override
	public int compareTo(DocumentPage otherDocument) {
		final CompareToBuilder compareToBuilder = new CompareToBuilder();
		compareToBuilder.append(this.fileName, otherDocument.fileName);
		compareToBuilder.append(this.directory, otherDocument.directory);
		compareToBuilder.append(this.typeDocument, otherDocument.typeDocument);
		compareToBuilder.append(this.listPages, otherDocument.listPages);
		return compareToBuilder.toComparison();
	}
	
	@Override
	public int hashCode() {
		final HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
		hashCodeBuilder.append(fileName);
		hashCodeBuilder.append(directory);
		hashCodeBuilder.append(typeDocument);
		hashCodeBuilder.append(listPages);
		return hashCodeBuilder.toHashCode();
	}
	
	@Override
	public boolean equals(Object object) {
		boolean isEqual = false;
		if (this == object) {
			isEqual = true;
		} else if (null == object) {
			isEqual = false;
		} else if (object instanceof DocumentPage) {
			final EqualsBuilder equalsBuilder = new EqualsBuilder();
			final DocumentPage documentPage = (DocumentPage) object;
			equalsBuilder.append(this.fileName, documentPage.fileName);
			equalsBuilder.append(this.directory, documentPage.directory);
			equalsBuilder.append(this.typeDocument, documentPage.typeDocument);
			equalsBuilder.append(this.listPages, documentPage.listPages);
			isEqual = equalsBuilder.isEquals();
		} else {
			isEqual = false;
		}
		return isEqual;
	}
	
}
