package br.com.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import br.com.dto.MainDTO;
import br.com.enuns.DocumentTypeReportEnum;
import br.com.enuns.ExtractType;
import br.com.manager.DocumentManager;
import br.com.manager.InterfaceManager;
import br.com.manager.MessageManager;
import br.com.manager.PDFManager;
import br.com.manager.SystemManager;
import br.com.model.DocumentPage;
import br.com.pdfbox.PDFSample;
import br.com.utils.ConstantsUtils;

/**
 * 
 * @author bruno.dourado
 *
 */
public class ViewUI {

	private JFrame frmExtractor;
	private JTextField textImportPath;
	private JTextField textDirectoryPath;
	private JTextField textImportPathDefault;
	private JTextField textNameCopy;
	private JTextField textOriginalDocument;
	private JComboBox<String> comboBoxDocumentType;
	private JTable tableExtract;
	private JTextField textExtractPath;
	private JButton btnOpenExtractFolder;
	private JTextField textDocumentCopy;
	private JButton btnOpenCopy;
	private JButton btnOpenFolderCopy;
	private JRadioButton rdbtnExtractOnly;
	private JRadioButton rdbtnExtractInterval;
	private ButtonGroup typeManualGroup;
	private JRadioButton rdbtnExtractMultiple;
	private JTextField textMultiplePage;
	private JSpinner spinnerOnly;
	private JSpinner spinnerIntervalBegin;
	private JSpinner spinnerIntervalEnd;
	private JTable tableManualExtract;
	private JButton button;
	private JButton btnOpenExtractFolderManual;
	private JTextField textExtractPathManual;
	private JLabel label_1;
	private JPanel panelFile;
	private JTextField textNewFileName;
	private JLabel lblNewFileName;
	private JLabel lblNewFile;
	private JButton btnNewFile;
	private JScrollPane scrollPaneFileContent;
	private JTextArea textAreaFileContent;
	private JTextField textAbsoluteNewFile;
	private JButton btnOpenNewFile;
	private JButton btnOpenFolderNewFile;
	private JButton button_1;
	
	/**
	 * Create the application.
	 */
	public ViewUI(MainDTO mainDTO) {
		initialize(mainDTO);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(MainDTO mainDTO) {
		initializeLooAndFell();
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 500, 600);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		
		JLabel lblExtractor = new JLabel("Extractor");
		lblExtractor.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblExtractor.setBounds(210, 11, 91, 25);
		getFrame().getContentPane().add(lblExtractor);
		
		JButton btnImportarDocumento = new JButton("Importar Documento");
		btnImportarDocumento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = mainDTO.getInterfaceManager().fileChooserPdf(getFrame());
				if(null != file) {
					mainDTO.setImportFile(file);
					mainDTO.getPdfManager().setFilePath(mainDTO.getImportFile().getAbsolutePath());
					MessageManager.showMessageDialogLoading(getFrame());
					mainDTO.getPdfManager().importFile(getFrame());
					getTextImportPath().setText(mainDTO.getImportFile().getAbsolutePath());
					getTextOriginalDocument().setText(mainDTO.getImportFile().getName());
					getFrame().setEnabled(true);
				}
			}
		});
		btnImportarDocumento.setBounds(10, 45, 166, 27);
		getFrame().getContentPane().add(btnImportarDocumento);
		
		setTextImportPath(new JTextField());
		getTextImportPath().setBackground(Color.WHITE);
		getTextImportPath().setEditable(false);
		getTextImportPath().setBounds(179, 45, 305, 27);
		getFrame().getContentPane().add(getTextImportPath());
		getTextImportPath().setColumns(10);
		{
			JButton btnPastaDestino = new JButton("Pasta Destino");
			btnPastaDestino.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String directoryChooserPath = mainDTO.getInterfaceManager().directoryChooser(getFrame());
					getTextDirectoryPath().setText(directoryChooserPath);
					mainDTO.getInterfaceManager().setDirectoryPath(directoryChooserPath);
				}
			});
			btnPastaDestino.setBounds(10, 120, 166, 27);
			frmExtractor.getContentPane().add(btnPastaDestino);
		}
		{
			setTextDirectoryPath(new JTextField());
			getTextDirectoryPath().setBackground(Color.WHITE);
			getTextDirectoryPath().setEditable(false);
			getTextDirectoryPath().setBounds(179, 120, 305, 27);
			frmExtractor.getContentPane().add(getTextDirectoryPath());
			getTextDirectoryPath().setColumns(10);
		}
		{
			JButton btnNewButton = new JButton("Caminho Padr�o");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String directoryChooserPath = mainDTO.getInterfaceManager().directoryChooser(getFrame());
					getTextImportPathDefault().setText(directoryChooserPath);
					mainDTO.getInterfaceManager().setDefaultPath(directoryChooserPath);
				}
			});
			btnNewButton.setBounds(10, 83, 166, 26);
			frmExtractor.getContentPane().add(btnNewButton);
		}
		{
			setTextImportPathDefault(new JTextField());
			getTextImportPathDefault().setBackground(Color.WHITE);
			getTextImportPathDefault().setEditable(false);
			getTextImportPathDefault().setBounds(179, 83, 305, 26);
			frmExtractor.getContentPane().add(getTextImportPathDefault());
			getTextImportPathDefault().setColumns(10);
		}
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 158, 474, 402);
		frmExtractor.getContentPane().add(tabbedPane);
		{
			JPanel panelBasic = new JPanel();
			panelBasic.setBackground(Color.LIGHT_GRAY);
			tabbedPane.addTab("B�sico", null, panelBasic, null);
			panelBasic.setLayout(null);
			{
				JButton btnCopiarDocumento = new JButton("Copiar Documento");
				btnCopiarDocumento.setBounds(287, 92, 172, 29);
				panelBasic.add(btnCopiarDocumento);
				
				setTextNameCopy(new JTextField());
				getTextNameCopy().setBounds(10, 92, 272, 29);
				panelBasic.add(getTextNameCopy());
				getTextNameCopy().setColumns(10);
				
				JLabel lblNomeDaCopia = new JLabel("Nome da copia:");
				lblNomeDaCopia.setLabelFor(textNameCopy);
				lblNomeDaCopia.setBounds(10, 73, 272, 14);
				panelBasic.add(lblNomeDaCopia);
				{
					JLabel lblDocumentoOriginal = new JLabel("Documento original:");
					lblDocumentoOriginal.setBounds(10, 11, 272, 14);
					panelBasic.add(lblDocumentoOriginal);
				}
				{
					setTextOriginalDocument(new JTextField());
					getTextOriginalDocument().setEditable(false);
					getTextOriginalDocument().setColumns(10);
					getTextOriginalDocument().setBounds(10, 33, 272, 29);
					panelBasic.add(getTextOriginalDocument());
				}
				
				JButton btnAbrirDocumento = new JButton("Abrir Documento");
				btnAbrirDocumento.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(MessageManager.showMessageDialogImport(mainDTO, getFrame())) {
							try {
								SystemManager.openFile(mainDTO.getImportFile(), getFrame());
							} catch (IOException e1) {
								MessageManager.showExceptionDialogError(getFrame());
							}
						}
					}
				});
				btnAbrirDocumento.setBounds(287, 33, 172, 29);
				panelBasic.add(btnAbrirDocumento);
				{
					setTextDocumentCopy(new JTextField());
					getTextDocumentCopy().setEditable(false);
					getTextDocumentCopy().setColumns(10);
					getTextDocumentCopy().setBounds(10, 152, 185, 29);
					panelBasic.add(getTextDocumentCopy());
				}
				{
					JLabel lblDocumentoCopiado = new JLabel("Documento copiado:");
					lblDocumentoCopiado.setBounds(10, 132, 272, 14);
					panelBasic.add(lblDocumentoCopiado);
				}
				{
					setBtnOpenCopy(new JButton("Abrir Documento"));
					getBtnOpenCopy().addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								SystemManager.openFile(getTextDocumentCopy().getText(), getFrame());
							} catch (IOException e1) {
								MessageManager.showExceptionDialogError(getFrame());
							}
						}
					});
					getBtnOpenCopy().setEnabled(false);
					getBtnOpenCopy().setBounds(205, 152, 122, 29);
					panelBasic.add(getBtnOpenCopy());
				}
				{
					setBtnOpenFolderCopy(new JButton("Abrir Pasta"));
					getBtnOpenFolderCopy().setEnabled(false);
					getBtnOpenFolderCopy().addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								SystemManager.openFolder(mainDTO.getInterfaceManager().getCopyPath());
							} catch (IOException e1) {
								MessageManager.showExceptionDialogError(getFrame());
							}
						}
					});
					getBtnOpenFolderCopy().setBounds(337, 152, 122, 29);
					panelBasic.add(getBtnOpenFolderCopy());
				}
				btnCopiarDocumento.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(MessageManager.showMessageDialogImport(mainDTO, getFrame())) {
							MessageManager.showMessageDialogLoading(getFrame());
							try {
								mainDTO.getPdfManager().loadDocument(mainDTO.getImportFile(), getFrame());
							} catch (IOException e1) {
								MessageManager.showExceptionDialogError(getFrame());
							}
							String newFileName = PDFManager.getCopyNameDocument(
									textNameCopy.getText(), mainDTO.getImportFile().getName());
							String path = mainDTO.getInterfaceManager().getDirectoryPath();
							try {
								mainDTO.getPdfManager().createFile(newFileName, path);
								MessageManager.showMessageDialogSuccess(getFrame(), "Copia conclu�da com sucesso!");
								getTextDocumentCopy().setText(PDFManager.getAbsolutePath(path, newFileName));
								mainDTO.getInterfaceManager().setCopyPath(PDFManager.getPath(path));
								getBtnOpenCopy().setEnabled(true);
								getBtnOpenFolderCopy().setEnabled(true);
							} catch (IOException e1) {
								MessageManager.showExceptionDialogError(getFrame());
							}
							getFrame().setEnabled(true);
						}
					}
				});
			}
		}
		{
			setTypeManualGroup(new ButtonGroup());
			JPanel panelManual = new JPanel();
			panelManual.setBackground(Color.LIGHT_GRAY);
			tabbedPane.addTab("Manual", null, panelManual, null);
			panelManual.setLayout(null);
			
			setRdbtnExtractOnly(new JRadioButton("Extrair uma \u00FAnica p\u00E1gina"));
			getRdbtnExtractOnly().setBackground(Color.LIGHT_GRAY);
			getRdbtnExtractOnly().setBounds(6, 7, 187, 23);
			getRdbtnExtractOnly().addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					spinnerOnly.setEnabled(true);
					spinnerIntervalBegin.setValue(1);
					spinnerIntervalBegin.setEnabled(false);
					spinnerIntervalEnd.setValue(1);
					spinnerIntervalEnd.setEnabled(false);
					textMultiplePage.setText("");
					textMultiplePage.setEnabled(false);
					mainDTO.getInterfaceManager().setExtractType(ExtractType.ONLY);
				}
			});
			panelManual.add(getRdbtnExtractOnly());
			{
				setRdbtnExtractInterval(new JRadioButton("Extrair p�ginas por intervalo"));
				getRdbtnExtractInterval().setBackground(Color.LIGHT_GRAY);
				getRdbtnExtractInterval().setBounds(6, 46, 187, 23);
				getRdbtnExtractInterval().addItemListener(new ItemListener() {
					public void itemStateChanged(ItemEvent e) {
						spinnerOnly.setValue(1);
						spinnerOnly.setEnabled(false);
						spinnerIntervalBegin.setEnabled(true);
						spinnerIntervalEnd.setEnabled(true);
						textMultiplePage.setText("");
						textMultiplePage.setEnabled(false);
						mainDTO.getInterfaceManager().setExtractType(ExtractType.INTERVAL);
					}
				});
				panelManual.add(getRdbtnExtractInterval());
			}
			{
				setRdbtnExtractMultiple(new JRadioButton("Extrair m�ltiplas p�ginas"));
				getRdbtnExtractMultiple().setBackground(Color.LIGHT_GRAY);
				getRdbtnExtractMultiple().setBounds(6, 87, 187, 23);
				getRdbtnExtractMultiple().addItemListener(new ItemListener() {
					public void itemStateChanged(ItemEvent e) {
						spinnerOnly.setValue(1);
						spinnerOnly.setEnabled(false);
						spinnerIntervalBegin.setValue(1);
						spinnerIntervalBegin.setEnabled(false);
						spinnerIntervalBegin.setValue(1);
						spinnerIntervalEnd.setEnabled(false);
						textMultiplePage.setEnabled(true);
						mainDTO.getInterfaceManager().setExtractType(ExtractType.MULTIPLE);
					}
				});
				panelManual.add(getRdbtnExtractMultiple());
			}
			getTypeManualGroup().add(getRdbtnExtractOnly());
			getTypeManualGroup().add(getRdbtnExtractInterval());
			getTypeManualGroup().add(getRdbtnExtractMultiple());
			mainDTO.getInterfaceManager().setExtractType(ExtractType.ONLY);
			
			spinnerOnly = new JSpinner();
			spinnerOnly.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
			spinnerOnly.setBounds(199, 8, 67, 28);
			panelManual.add(spinnerOnly);
			
			spinnerIntervalBegin = new JSpinner();
			spinnerIntervalBegin.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
			spinnerIntervalBegin.setEnabled(false);
			spinnerIntervalBegin.setBounds(199, 47, 67, 29);
			panelManual.add(spinnerIntervalBegin);
			
			spinnerIntervalEnd = new JSpinner();
			spinnerIntervalEnd.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
			spinnerIntervalEnd.setEnabled(false);
			spinnerIntervalEnd.setBounds(276, 47, 67, 29);
			panelManual.add(spinnerIntervalEnd);
			
			textMultiplePage = new JTextField();
			textMultiplePage.setEnabled(false);
			textMultiplePage.setBounds(199, 87, 260, 28);
			panelManual.add(textMultiplePage);
			textMultiplePage.setColumns(10);
			
			JLabel lblSepareAs = new JLabel("* Separe os n�meros por v�rgula [ , ]");
			lblSepareAs.setBounds(199, 114, 210, 14);
			panelManual.add(lblSepareAs);
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 197, 449, 166);
			panelManual.add(scrollPane);
			
			tableManualExtract = new JTable();
			tableManualExtract.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Endere\u00E7o", "Nome", "A\u00E7\u00E3o"
				}
			));
			tableManualExtract.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableManualExtract.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			tableManualExtract.setModel(InterfaceManager.createManualExtractTable());
			tableManualExtract.getColumnModel().getColumn(1).setPreferredWidth(122);
			tableManualExtract.getTableHeader().setReorderingAllowed(false);
			tableManualExtract.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent e){
					int row = tableManualExtract.getSelectedRow();
					int column = tableManualExtract.getSelectedColumn();
					if(column == 2){
						String fileName = (String) tableManualExtract.getModel().getValueAt(row, 1);
						try {
							SystemManager.openFile(
									PDFManager.getAbsolutePath(
											mainDTO.getInterfaceManager().getManualExtractPath(), fileName), getFrame());
						} catch (IOException e1) {
							MessageManager.showExceptionDialogError(getFrame());
						}
					}
				}
			});
			scrollPane.setViewportView(tableManualExtract);
			{
				button = new JButton("Extrair");
				button.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(MessageManager.showMessageDialogImport(mainDTO, getFrame())) {
							MessageManager.showMessageDialogLoading(getFrame());
							List<DocumentPage> listDocumentPage = new ArrayList<>();
							try {
								listDocumentPage = DocumentManager.extract(mainDTO, 
										(Integer)spinnerOnly.getValue(), (Integer)spinnerIntervalBegin.getValue(), 
										(Integer)spinnerIntervalEnd.getValue(), textMultiplePage.getText(), 
										getFrame());
							
								if(!listDocumentPage.isEmpty()) {
									InterfaceManager.createManualExtractRows(tableManualExtract, listDocumentPage);
									String extractPath = PDFManager.getPath(mainDTO.getInterfaceManager().getDirectoryPath());
									textExtractPathManual.setText(extractPath);
									mainDTO.getInterfaceManager().setManualExtractPath(extractPath);
									btnOpenExtractFolderManual.setEnabled(true);
									MessageManager.showMessageDialogSuccess(getFrame(), "Extra��o conclu�da com sucesso!");
								} else {
									MessageManager.showMessageDialogSuccess(getFrame(), "Nenhuma p�gina foi extra�da!");
								}
							} catch (Exception e1) {
								MessageManager.showExceptionDialogError(getFrame());
							} 
							
							getFrame().setEnabled(true);
						}
					}
				});
				button.setBounds(319, 7, 140, 29);
				panelManual.add(button);
			}
			{
				btnOpenExtractFolderManual = new JButton("Abrir Pasta");
				btnOpenExtractFolderManual.setEnabled(false);
				btnOpenExtractFolderManual.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							SystemManager.openFolder(mainDTO.getInterfaceManager().getManualExtractPath());
						} catch (IOException e1) {
							MessageManager.showExceptionDialogError(getFrame());
						}
					}
				});
				btnOpenExtractFolderManual.setBounds(284, 157, 175, 29);
				panelManual.add(btnOpenExtractFolderManual);
			}
			{
				textExtractPathManual = new JTextField();
				textExtractPathManual.setEditable(false);
				textExtractPathManual.setColumns(10);
				textExtractPathManual.setBounds(10, 157, 268, 29);
				panelManual.add(textExtractPathManual);
			}
			{
				label_1 = new JLabel("Diret\u00F3rio dos arquivos extra\u00EDdos:");
				label_1.setBounds(10, 135, 268, 14);
				panelManual.add(label_1);
			}
		}
		{
			JPanel panelAutomatic = new JPanel();
			panelAutomatic.setBackground(Color.LIGHT_GRAY);
			tabbedPane.addTab("Autom�tico", null, panelAutomatic, null);
			panelAutomatic.setLayout(null);
			{
				JButton btnExtrair = new JButton("Extrair");
				btnExtrair.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(MessageManager.showMessageDialogImport(mainDTO, getFrame())) {
							MessageManager.showMessageDialogLoading(getFrame());
							List<DocumentPage> listDocumentPage = DocumentManager.extract(mainDTO, getFrame());
							if(!listDocumentPage.isEmpty()) {
								InterfaceManager.createExtractRows(tableExtract, listDocumentPage);
								String extractPath = PDFManager.getPath(mainDTO.getInterfaceManager().getDirectoryPath());
								getTextExtractPath().setText(extractPath);
								mainDTO.getInterfaceManager().setExtractPath(extractPath);
								btnOpenExtractFolder.setEnabled(true);
								MessageManager.showMessageDialogSuccess(getFrame(), "Extra��o conclu�da com sucesso!");
							} else {
								MessageManager.showMessageDialogSuccess(getFrame(), "Nenhuma p�gina foi extra�da!");
							}
							
							getFrame().setEnabled(true);
						}
					}
				});
				btnExtrair.setBounds(193, 11, 89, 30);
				panelAutomatic.add(btnExtrair);
			}
			{
				JLabel lblTipoDeDocumento = new JLabel("Tipo de Documento:");
				lblTipoDeDocumento.setBounds(10, 44, 119, 14);
				panelAutomatic.add(lblTipoDeDocumento);
			}
			
			setComboBoxDocumentType(new JComboBox<String>());
			for (DocumentTypeReportEnum documentTypeReportEnum : DocumentTypeReportEnum.getList()) {
				getComboBoxDocumentType().addItem(documentTypeReportEnum.getDescription());
			}
			getComboBoxDocumentType().setBounds(10, 69, 119, 30);
			mainDTO.setDocumentTypeReportEnum(DocumentTypeReportEnum.DETALHADO);
			getComboBoxDocumentType().addItemListener(new ItemListener() {
	            public void itemStateChanged(ItemEvent e) {
	                if(e.getStateChange() == ItemEvent.SELECTED)
	                	mainDTO.setDocumentTypeReportEnum(
	                			DocumentTypeReportEnum.getByDescription(e.getItem().toString()));
	            }
	        });
			
			panelAutomatic.add(getComboBoxDocumentType());
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 175, 449, 188);
			panelAutomatic.add(scrollPane);
			{
				tableExtract = new JTable();
				tableExtract.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				tableExtract.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
				tableExtract.setModel(InterfaceManager.createExtractTable());
				tableExtract.getColumnModel().getColumn(1).setPreferredWidth(122);
				tableExtract.getTableHeader().setReorderingAllowed(false);
				tableExtract.addMouseListener(new MouseAdapter(){
					@Override
					public void mouseClicked(MouseEvent e){
						int row = tableExtract.getSelectedRow();
						int column = tableExtract.getSelectedColumn();
						if(column == 2){
							String fileName = (String) tableExtract.getModel().getValueAt(row, 1);
							try {
								SystemManager.openFile(
										PDFManager.getAbsolutePath(
												mainDTO.getInterfaceManager().getExtractPath(), fileName), getFrame());
							} catch (IOException e1) {
								MessageManager.showExceptionDialogError(getFrame());
							}
						}
					}
				});
				scrollPane.setViewportView(tableExtract);
			}
			
			setTextExtractPath(new JTextField());
			getTextExtractPath().setEditable(false);
			getTextExtractPath().setColumns(10);
			getTextExtractPath().setBounds(10, 135, 272, 29);
			panelAutomatic.add(getTextExtractPath());
			
			JLabel label = new JLabel("Diret�rio dos arquivos extra�dos:");
			label.setBounds(10, 110, 272, 14);
			panelAutomatic.add(label);
			
			btnOpenExtractFolder = new JButton("Abrir Pasta");
			btnOpenExtractFolder.setEnabled(false);
			btnOpenExtractFolder.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						SystemManager.openFolder(getTextExtractPath().getText());
					} catch (IOException e1) {
						MessageManager.showExceptionDialogError(getFrame());
					}
				}
			});
			btnOpenExtractFolder.setBounds(288, 135, 171, 29);
			panelAutomatic.add(btnOpenExtractFolder);
		}
		{
			panelFile = new JPanel();
			panelFile.setBackground(Color.LIGHT_GRAY);
			tabbedPane.addTab("Arquivo", null, panelFile, null);
			panelFile.setLayout(null);
			{
				textNewFileName = new JTextField();
				textNewFileName.setColumns(10);
				textNewFileName.setBounds(10, 66, 272, 29);
				panelFile.add(textNewFileName);
			}
			{
				lblNewFileName = new JLabel("Nome do novo arquivo:");
				lblNewFileName.setBounds(10, 46, 272, 14);
				panelFile.add(lblNewFileName);
			}
			{
				lblNewFile = new JLabel("Criar arquivo");
				lblNewFile.setFont(new Font("Tahoma", Font.BOLD, 14));
				lblNewFile.setBounds(10, 11, 185, 24);
				panelFile.add(lblNewFile);
			}
			{
				btnNewFile = new JButton("Criar Arquivo");
				btnNewFile.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						MessageManager.showMessageDialogLoading(getFrame());
						String path = mainDTO.getInterfaceManager().getDirectoryPath();
						String absolutePath = PDFManager.createNewFile(path, textNewFileName.getText(), getFrame(), 
								textAreaFileContent.getText());
						if(null != absolutePath) {
							mainDTO.getInterfaceManager().setNewFilePath(PDFManager.getPath(path));
							textAbsoluteNewFile.setText(absolutePath);
							btnOpenNewFile.setEnabled(true);
							btnOpenFolderNewFile.setEnabled(true);
						}
						getFrame().setEnabled(true);
					}
				});
				btnNewFile.setBounds(287, 66, 172, 29);
				panelFile.add(btnNewFile);
			}
			
			JLabel lblContedoDoArquivo = new JLabel("Conte\u00FAdo do arquivo:");
			lblContedoDoArquivo.setBounds(10, 106, 272, 14);
			panelFile.add(lblContedoDoArquivo);
			
			textAreaFileContent = new JTextArea();
			textAreaFileContent.setEditable(true); 
			textAreaFileContent.setLineWrap(true);
			textAreaFileContent.setBounds(10, 131, 449, 84);
			{
				scrollPaneFileContent = new JScrollPane();
				scrollPaneFileContent.setViewportBorder(UIManager.getBorder("TextArea.border"));
				scrollPaneFileContent.setBounds(10, 131, 449, 170);
				scrollPaneFileContent.setViewportView(textAreaFileContent);
				panelFile.add(scrollPaneFileContent);
			}
			{
				textAbsoluteNewFile = new JTextField();
				textAbsoluteNewFile.setEditable(false);
				textAbsoluteNewFile.setColumns(10);
				textAbsoluteNewFile.setBounds(10, 308, 185, 29);
				panelFile.add(textAbsoluteNewFile);
			}
			{
				btnOpenNewFile = new JButton("Abrir Arquivo");
				btnOpenNewFile.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							SystemManager.openFile(new File(textAbsoluteNewFile.getText()), getFrame());
						} catch (IOException e1) {
							MessageManager.showExceptionDialogError(getFrame());
						}
					}
				});
				btnOpenNewFile.setEnabled(false);
				btnOpenNewFile.setBounds(205, 308, 122, 29);
				panelFile.add(btnOpenNewFile);
			}
			{
				btnOpenFolderNewFile = new JButton("Abrir Pasta");
				btnOpenFolderNewFile.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							SystemManager.openFolder(mainDTO.getInterfaceManager().getNewFilePath());
						} catch (IOException e1) {
							MessageManager.showExceptionDialogError(getFrame());
						}
					}
				});
				btnOpenFolderNewFile.setEnabled(false);
				btnOpenFolderNewFile.setBounds(337, 308, 122, 29);
				panelFile.add(btnOpenFolderNewFile);
			}
			{
				button_1 = new JButton("Criar Tabela");
				button_1.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							//PDFManager.createTable();
							PDFSample.createSample();
						} catch (IOException e1) {
							MessageManager.showExceptionDialogError(getFrame());
						}
					}
				});
				button_1.setBounds(287, 14, 172, 29);
				panelFile.add(button_1);
			}
		}
	}
	
	private void initializeLooAndFell() {
		try {
			for(javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if(ConstantsUtils.LOOK_AND_FELL.equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException 
			   | InstantiationException 
			   | IllegalAccessException 
			   |javax.swing.UnsupportedLookAndFeelException e) {
			MessageManager.showExceptionDialogError(getFrame());
		}
	}

	public JTextField getTextImportPath() {
		return textImportPath;
	}

	public void setTextImportPath(JTextField textImportPath) {
		this.textImportPath = textImportPath;
	}

	public JFrame getFrame() {
		return frmExtractor;
	}

	public void setFrame(JFrame frame) {
		this.frmExtractor = frame;
		frmExtractor.setIconImage(Toolkit.getDefaultToolkit().getImage(ViewUI.class.getResource("/br/com/image/application_icon_2.png")));
		frmExtractor.setResizable(false);
		frmExtractor.setTitle("Extractor");
	}

	public JTextField getTextDirectoryPath() {
		return textDirectoryPath;
	}

	public void setTextDirectoryPath(JTextField textDirectoryPath) {
		this.textDirectoryPath = textDirectoryPath;
	}

	public JTextField getTextImportPathDefault() {
		return textImportPathDefault;
	}

	public void setTextImportPathDefault(JTextField textImportPathDefault) {
		this.textImportPathDefault = textImportPathDefault;
	}

	public JTextField getTextNameCopy() {
		return textNameCopy;
	}

	public void setTextNameCopy(JTextField textNameCopy) {
		this.textNameCopy = textNameCopy;
	}

	public JTextField getTextOriginalDocument() {
		return textOriginalDocument;
	}

	public void setTextOriginalDocument(JTextField textOriginalDocument) {
		this.textOriginalDocument = textOriginalDocument;
	}

	public JComboBox<String> getComboBoxDocumentType() {
		return comboBoxDocumentType;
	}

	public void setComboBoxDocumentType(JComboBox<String> comboBoxDocumentType) {
		this.comboBoxDocumentType = comboBoxDocumentType;
	}

	public JTextField getTextExtractPath() {
		return textExtractPath;
	}

	public void setTextExtractPath(JTextField textExtractPath) {
		this.textExtractPath = textExtractPath;
	}

	public JTextField getTextDocumentCopy() {
		return textDocumentCopy;
	}

	public void setTextDocumentCopy(JTextField textDocumentCopy) {
		this.textDocumentCopy = textDocumentCopy;
	}

	public JButton getBtnOpenCopy() {
		return btnOpenCopy;
	}

	public void setBtnOpenCopy(JButton btnOpenCopy) {
		this.btnOpenCopy = btnOpenCopy;
	}

	public JButton getBtnOpenFolderCopy() {
		return btnOpenFolderCopy;
	}

	public void setBtnOpenFolderCopy(JButton btnOpenFolderCopy) {
		this.btnOpenFolderCopy = btnOpenFolderCopy;
	}
	
	public JRadioButton getRdbtnExtractOnly() {
		return rdbtnExtractOnly;
	}

	public void setRdbtnExtractOnly(JRadioButton rdbtnExtractOnly) {
		this.rdbtnExtractOnly = rdbtnExtractOnly;
		rdbtnExtractOnly.setSelected(true);
	}

	public JRadioButton getRdbtnExtractInterval() {
		return rdbtnExtractInterval;
	}

	public void setRdbtnExtractInterval(JRadioButton rdbtnExtractInterval) {
		this.rdbtnExtractInterval = rdbtnExtractInterval;
	}

	public ButtonGroup getTypeManualGroup() {
		return typeManualGroup;
	}

	public void setTypeManualGroup(ButtonGroup typeManualGroup) {
		this.typeManualGroup = typeManualGroup;
	}

	public JRadioButton getRdbtnExtractMultiple() {
		return rdbtnExtractMultiple;
	}

	public void setRdbtnExtractMultiple(JRadioButton rdbtnExtractMultiple) {
		this.rdbtnExtractMultiple = rdbtnExtractMultiple;
	}
}
