package br.com.enuns;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author bruno.dourado
 *
 */
public enum DocumentTypeReportEnum {
	
	DETALHADO (0, "Detalhado", "_detalhado"),
	RESUMIDO  (1, "Resumido", "_resumido"  );
    
    private final int code;
    private final String description;
    private final String descriptionSpace;
    
    DocumentTypeReportEnum(int code, String description, String descriptionSpace) {
    	this.code = code;
    	this.description = description;
    	this.descriptionSpace = descriptionSpace;
    }
    
    public int getCode() {
        return code;
    }
    
    public String getDescription() {
        return description;
    }

	public String getDescriptionSpace() {
		return descriptionSpace;
	}
	
	public static DocumentTypeReportEnum getByDescription(String description) {
		for (DocumentTypeReportEnum documentTypeReportEnum : DocumentTypeReportEnum.getList()) {
			if (documentTypeReportEnum.getDescription().compareTo(description) == 0) {
				return documentTypeReportEnum;
			}
		}
		return null;
	}
	
	public static List<DocumentTypeReportEnum> getList() {
    	return Arrays.asList(DocumentTypeReportEnum.values());
    }
    
}
