package br.com.pdfbox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

public class PDFSample {
	// Page configuration
    private static final PDRectangle PAGE_SIZE = PDRectangle.A4;
    private static final float MARGIN = 20;
    private static final boolean IS_LANDSCAPE = true;

    // Font configuration
    private static final PDFont TEXT_FONT = PDType1Font.HELVETICA;
    private static final float FONT_SIZE = 10;

    // Table configuration
    private static final float ROW_HEIGHT = 15;
    private static final float CELL_MARGIN = 2;
    
    public static void createSample() throws IOException {
        new PDFTableGenerator().generatePDF(createContent());
    }

    private static Table createContent() {
        // Total size of columns must not be greater than table width.
        List<Column> columns = new ArrayList<Column>();
        columns.add(new Column("Tipo", 90));
        columns.add(new Column("CGC", 90));
        columns.add(new Column("Unidade", 90));
        columns.add(new Column("SN", 90));
        columns.add(new Column("DI", 90));
        columns.add(new Column("VP", 90));
        columns.add(new Column("Aceitas", 90));
        columns.add(new Column("Recusadas", 90));
        columns.add(new Column("Total", 90));

        String[][] content = { 
                { "SC", "5428", "SEGER",     " ",     " ",     " ", "10", "5", "50,00" },
                { "GN", "3548", "GEOTI", "SOUTI", "DETEC", "VITEC", "15", "6", "60,00" },
                { "DI", "1548", "DETEC",     " ", "DETEC", "VITEC", "20", "8", "60,00" }
        };

        float tableHeight = IS_LANDSCAPE ? PAGE_SIZE.getWidth() - (2 * MARGIN) : PAGE_SIZE.getHeight() - (2 * MARGIN);

        Table table = new TableBuilder()
            .setCellMargin(CELL_MARGIN)
            .setColumns(columns)
            .setContent(content)
            .setHeight(tableHeight)
            .setNumberOfRows(content.length)
            .setRowHeight(ROW_HEIGHT)
            .setMargin(MARGIN)
            .setPageSize(PAGE_SIZE)
            .setLandscape(IS_LANDSCAPE)
            .setTextFont(TEXT_FONT)
            .setFontSize(FONT_SIZE)
            .build();
        return table;
    }   
}
