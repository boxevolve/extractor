package br.com.manager;

import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import br.com.dto.MainDTO;
import br.com.enuns.DocumentTypeEnum;
import br.com.enuns.DocumentTypeReportEnum;
import br.com.model.DocumentPage;
import br.com.utils.ConstantsUtils;

/**
 * 
 * @author bruno.dourado
 *
 */
public class DocumentManager {
	
	private DocumentManager() {
		super();
	}
	
	/**
	 * Responsavel por separar e extrair automaticamente por centros de custos.
	 * 
	 * @param mainDTO
	 * @param parent
	 * @return
	 */
	public static List<DocumentPage> extract(MainDTO mainDTO, Component parent) {
		List<DocumentPage> listDocumentPage = new ArrayList<>();
		try {
			listDocumentPage = DocumentManager.extractDocument(mainDTO, parent);
			DocumentManager.createDocuments(mainDTO, listDocumentPage, mainDTO.getDocumentTypeReportEnum());
		} catch (Exception e) {
			MessageManager.showExceptionDialogError(parent);
		}
		Collections.sort(listDocumentPage);
		return listDocumentPage;
	}
	
	/**
	 * Responsavel por extrair paginas selecionadas pela forma que o usuario selecionar.
	 * 
	 * @param mainDTO
	 * @param onlyPage
	 * @param beginPage
	 * @param endPage
	 * @param multiplePage
	 * @param parent
	 * @return
	 * @throws IOException 
	 */
	public static List<DocumentPage> extract(MainDTO mainDTO, Integer onlyPage, Integer beginPage, Integer endPage, 
			String multiplePage, Component parent) throws IOException {
		mainDTO.getPdfManager().loadDocument(mainDTO.getImportFile(), parent);
		
		List<DocumentPage> listDocumentPage = new ArrayList<>();
		List<Integer> listNumberPages = new ArrayList<>();
		List<PDPage> listPages = new ArrayList<>();
		
		String importFileName = PDFManager.removeExtension(mainDTO.getImportFile().getName());
		int importNumberPages = mainDTO.getPdfManager().getPdDoc().getNumberOfPages();
		
		String fileName = ConstantsUtils.EMPTY;
		String directory = PDFManager.getPath(mainDTO.getInterfaceManager().getDirectoryPath());
		
		switch (mainDTO.getInterfaceManager().getExtractType()) {
			case ONLY:
				fileName = extractOnlyPage(importFileName, onlyPage, parent, listNumberPages, importNumberPages);
				if(null == fileName) {
					return Collections.emptyList();
				}
				break;
			case INTERVAL:
				fileName = extractInterval(importFileName, beginPage, endPage, parent, listNumberPages, 
						importNumberPages);
				break;
			case MULTIPLE:
				fileName = extractMultiple(multiplePage, parent, listNumberPages, importFileName, importNumberPages);
				break;
			default:
				break;
		}
		listDocumentPage.add(new DocumentPage(fileName, directory, listNumberPages));
		for (DocumentPage documentPage : listDocumentPage) {
			for (Integer PDPage : documentPage.getListPages()) {
				listPages.add(mainDTO.getPdfManager().getPdDoc().getPage(PDPage));
			}
		}
		mainDTO.getPdfManager().createFile(directory, fileName, new PDDocument(), listPages);
		return listDocumentPage;
	}

	/**
	 * Responsavel por analisar e extrair uma unica pagina.
	 * 
	 * @param fileName
	 * @param onlyPage
	 * @param parent
	 * @param listNumberPages
	 * @param importNumberPages
	 * @return
	 */
	private static String extractOnlyPage(String fileName, Integer onlyPage, Component parent, 
			List<Integer> listNumberPages, int importNumberPages) {
		if (onlyPage-1 > importNumberPages) {
			MessageManager.showMessageDialogWarning(parent, ConstantsUtils.PAGE_NOT_FOUND);
			return null;
		}
		listNumberPages.add(onlyPage-1);
		return getFileNameExtractOnly(PDFManager.removeExtension(fileName), onlyPage);
	}
	
	/**
	 * Responsavel por analisar e extrair um intervalo de paginas.
	 * 
	 * @param fileName
	 * @param beginPage
	 * @param endPage
	 * @param parent
	 * @param numberPages
	 * @param importNumberPages
	 * @return
	 */
	private static String extractInterval(String fileName, Integer beginPage, Integer endPage, Component parent,
			List<Integer> listNumberPages, int importNumberPages) {
		if (endPage-1 > importNumberPages) {
			MessageManager.showMessageDialogWarning(parent, ConstantsUtils.PAGE_NOT_FOUND);
			return null;
		} else if (beginPage > endPage) {
			MessageManager.showMessageDialogWarning(parent, ConstantsUtils.INTERVAL_ERROR);
			return null;
		}
		for (int i = beginPage; i <= endPage; i++) {
			listNumberPages.add(i-1);
		}
		return getFileNameExtractInterval(fileName, beginPage, endPage);
	}
	
	/**
	 * Responsavel por analisar e extrar varias paginas escolhidas pelo usuario;
	 * 
	 * @param multiplePage
	 * @param parent
	 * @param listNumberPages
	 * @param importFileName
	 * @param importNumberPages
	 * @return
	 */
	private static String extractMultiple(String multiplePage, Component parent, List<Integer> listNumberPages,
			String importFileName, int importNumberPages) {
		String fileName;
		Set<String> listStrNumberPages = new HashSet<>(Arrays.asList(multiplePage.split(
				ConstantsUtils.FLAG_SPLIT_MULTIPLE)));
		
		fileName = getFileNameExtractMultiple(importFileName);
		for (String numberText : listStrNumberPages) {
			try {
				Integer number = new Integer(numberText);
				if(number-1 > importNumberPages) {
					MessageManager.showMessageDialogWarning(parent, ConstantsUtils.PAGE_NOT_FOUND);
					return null;
				}
				listNumberPages.add(number-1);
			} catch (Exception e) {
				MessageManager.showMessageDialogWarning(parent, ConstantsUtils.INVALID_FORMAT);
				return null;
			}
		}
		return fileName;
	}
	
	/**
	 * Analisa, separa e extrai as paginas por tipo de documento.
	 * 
	 * @param mainDTO
	 * @param parent
	 * @return
	 * @throws IOException
	 */
	private static List<DocumentPage> extractDocument(MainDTO mainDTO, Component parent) throws IOException {
		mainDTO.getPdfManager().loadDocument(mainDTO.getImportFile(), parent);
		
		List<DocumentPage> listDocumentPage = new ArrayList<>();
		int importNumberPages = mainDTO.getPdfManager().getPdDoc().getNumberOfPages();
		int index = 0;
		
		for (int i = 1; i <= importNumberPages; i++, index++) {
			DocumentTypeEnum documentType = null;
			String page = mainDTO.getPdfManager().toText(i, i, parent);
			List<String> listLines = Arrays.asList(page.split(ConstantsUtils.NEXT_LINE, -1));
			for (String line : listLines) {
				documentType = DocumentManager.validateDocument(line, mainDTO.getDocumentTypeReportEnum());
				if (null != documentType) {
					DocumentPage documentPage = new DocumentPage(documentType, new ArrayList<>());
					documentPage.getListPages().add(index);
					listDocumentPage.add(documentPage);
					break;
				}
			}
			if (null == documentType) {
				listDocumentPage.get(listDocumentPage.size()-1).getListPages().add(index);
			}
		}
		return DocumentManager.separateDocument(listDocumentPage);
	}
	
	/**
	 * Responsavel por varrer a linha do arquivo para saber de qual tipo � aquela pagina. 
	 * 
	 * @param line
	 * @param documentTypeReportEnum
	 * @return
	 */
	public static DocumentTypeEnum validateDocument(String line, DocumentTypeReportEnum documentTypeReportEnum) {
		DocumentTypeEnum documentType = null;
		List<String> words = Arrays.asList(line.split(ConstantsUtils.SPACE, -1));
		documentType = DocumentManager.validateWords(words, documentTypeReportEnum);
		return documentType;
	}

	/**
	 * Verifica e retorna qual � o tipo de documento.
	 * 
	 * @param documentType
	 * @param words
	 * @param documentTypeReportEnum
	 * @return
	 */
	private static DocumentTypeEnum validateWords(List<String> listWords, 
			DocumentTypeReportEnum documentTypeReportEnum) {
		boolean sameFlag = false;
		boolean sameDescription = false;
		Object[] documentType;
		
		for(String word : listWords) {
			documentType = getDocumentType(word, documentTypeReportEnum, sameFlag, sameDescription);
			if(null != documentType[2]) {
				return (DocumentTypeEnum) documentType[2];
			} else {
				sameFlag = (boolean) documentType[0];
				sameDescription = (boolean) documentType[1];
			}
		}
		return null;
	}
	
	/**
	 * Responsavel por validar cada palavra e verificar o tipo de documento.
	 * 
	 * @param word
	 * @param documentTypeReportEnum
	 * @param sameFlag
	 * @param sameDescription
	 * @return
	 */
	private static Object[] getDocumentType(String word, DocumentTypeReportEnum documentTypeReportEnum, 
			boolean sameFlag, boolean sameDescription) {
		DocumentTypeEnum documentTypeEnumFound = null;
		
		for (DocumentTypeEnum documentTypeEnum : DocumentTypeEnum.getList()) {
			String flagLine = documentTypeReportEnum.compareTo(DocumentTypeReportEnum.DETALHADO) == 0 
					? documentTypeEnum.getCode() : ConstantsUtils.FLAG_LINE_RESUMIDO;
					
			sameFlag = word.compareTo(flagLine) == 0 ? true : sameFlag;
			sameDescription = word.compareTo(documentTypeEnum.getDescription()) == 0 ? true : sameDescription;
			if(sameFlag && sameDescription) {
				documentTypeEnumFound = documentTypeEnum;
				break;
			}
		}
		return new Object[]{sameFlag, sameDescription, documentTypeEnumFound};
	}
	
	/**
	 * Responsavel por separar as paginas e juntar os tipos iguais.
	 * 
	 * @param listDocumentPage
	 * @return
	 */
	private static List<DocumentPage> separateDocument(List<DocumentPage> listDocumentPage) {
		List<DocumentPage> listSeparateDocumentPage = new ArrayList<>();
		List<DocumentPage> listCurrent = new ArrayList<>();
		
		for (DocumentTypeEnum TypeDocumentEnum : DocumentTypeEnum.getList()) {
			DocumentPage newDocumentPage = new DocumentPage(TypeDocumentEnum, new ArrayList<>());
			listCurrent.clear();
			listCurrent.addAll(listDocumentPage);
			for (DocumentPage documentPage : listCurrent) {
				if (documentPage.getTypeDocument().compareTo(TypeDocumentEnum) == 0) {
					newDocumentPage.getListPages().addAll(documentPage.getListPages());
					listDocumentPage.remove(documentPage);
				}
			}
			if (!newDocumentPage.getListPages().isEmpty()) {
				listSeparateDocumentPage.add(newDocumentPage);
			}
		}
		return listSeparateDocumentPage;
	}
	
	/**
	 * Responsavel por criar documentos com as paginas informadas.
	 * 
	 * @param mainDTO
	 * @param listDocumentPage
	 * @param documentType
	 * @throws IOException
	 */
	private static void createDocuments(MainDTO mainDTO, List<DocumentPage> listDocumentPage, 
			DocumentTypeReportEnum documentType) throws IOException {
		
		for (int i = 0; i < listDocumentPage.size(); i++) {
			List<PDPage> pages = new ArrayList<>();
			String fileName = DocumentManager.getFileName(documentType, listDocumentPage.get(i));
			String directory = mainDTO.getInterfaceManager().getDirectoryPath();
			
			for (Integer PDPage : listDocumentPage.get(i).getListPages()) {
				pages.add(mainDTO.getPdfManager().getPdDoc().getPage(PDPage));
			}
			
			mainDTO.getPdfManager().createFile(directory, fileName, new PDDocument(), pages);
			listDocumentPage.get(i).setFileName(fileName);
			listDocumentPage.get(i).setDirectory(PDFManager.getPath(directory));
		}
	}

	/**
	 * Responsavel por gerar um nome padr�o para a extra��o automatica.
	 * 
	 * @param documentType
	 * @param documentPage
	 * @return
	 */
	private static String getFileName(DocumentTypeReportEnum documentType, 
			DocumentPage documentPage) {
		StringBuilder fileName = new StringBuilder();
		fileName.append(documentPage.getTypeDocument().getDescription());
		fileName.append(documentType.getDescriptionSpace());
		fileName.append(ConstantsUtils.RAMAL);
		return fileName.toString();
	}
	
	/**
	 * Responsavel por gerar um nome padr�o para a extra��o manual de apenas uma pagina.
	 * 
	 * @param importFileName
	 * @param numberPage
	 * @return
	 */
	private static String getFileNameExtractOnly(String importFileName, Integer numberPage) {
		StringBuilder fileName = new StringBuilder();
		fileName.append(importFileName);
		fileName.append(ConstantsUtils.EXTRACT_ONLY);
		fileName.append("_");
		fileName.append(numberPage);
		return fileName.toString();
	}
	
	/**
	 * Responsavel por gerar um nome padr�o para a extra��o manual de um intervalo de paginas.
	 * 
	 * @param importFileName
	 * @param begin
	 * @param end
	 * @return
	 */
	private static String getFileNameExtractInterval(String importFileName, 
			Integer begin, Integer end) {
		StringBuilder fileName = new StringBuilder();
		fileName.append(importFileName);
		fileName.append(ConstantsUtils.EXTRACT_INTERVAL);
		fileName.append("_");
		fileName.append(begin);
		fileName.append("-");
		fileName.append(end);
		return fileName.toString();
	}
	
	/**
	 * Responsavel por gerar um nome padr�o para a extra��o manual de multiplas paginas.
	 * 
	 * @param importFileName
	 * @return
	 */
	private static String getFileNameExtractMultiple(String importFileName) {
		StringBuilder fileName = new StringBuilder();
		fileName.append(importFileName);
		fileName.append(ConstantsUtils.EXTRACT_MULTIPLE);
		return fileName.toString();
	}
	
}
