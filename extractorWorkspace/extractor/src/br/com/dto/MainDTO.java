package br.com.dto;

import java.io.File;
import java.util.List;

import br.com.enuns.DocumentTypeReportEnum;
import br.com.manager.InterfaceManager;
import br.com.manager.PDFManager;
import br.com.model.DocumentPage;

/**
 * 
 * @author bruno.dourado
 *
 */
public class MainDTO {
	
	private InterfaceManager interfaceManager;
	private PDFManager pdfManager;
	private File importFile;
	private DocumentTypeReportEnum documentTypeReportEnum;
	private List<DocumentPage> extractedDocument;
	
	public MainDTO() {
		super();
	}
	
	public MainDTO(InterfaceManager interfaceManager, PDFManager pdfManager) {
		this.interfaceManager = interfaceManager;
		this.pdfManager = pdfManager;
	}

	public InterfaceManager getInterfaceManager() {
		return interfaceManager;
	}

	public void setInterfaceManager(InterfaceManager interfaceManager) {
		this.interfaceManager = interfaceManager;
	}
	
	public PDFManager getPdfManager() {
		return pdfManager;
	}

	public void setPdfManager(PDFManager pdfManager) {
		this.pdfManager = pdfManager;
	}

	public File getImportFile() {
		return importFile;
	}

	public void setImportFile(File importFile) {
		this.importFile = importFile;
	}

	public DocumentTypeReportEnum getDocumentTypeReportEnum() {
		return documentTypeReportEnum;
	}

	public void setDocumentTypeReportEnum(DocumentTypeReportEnum documentTypeReportEnum) {
		this.documentTypeReportEnum = documentTypeReportEnum;
	}
	
	public List<DocumentPage> getExtractedDocument() {
		return extractedDocument;
	}

	public void setExtractedDocument(List<DocumentPage> extractedDocument) {
		this.extractedDocument = extractedDocument;
	}
	
}
